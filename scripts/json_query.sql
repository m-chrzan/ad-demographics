WITH targets AS (
    SELECT jsonb_array_elements(targets) AS target FROM targets_json
),
viewers AS (
    SELECT date, jsonb_array_elements(audience) AS viewer FROM audience_json
)
SELECT
    date,
    target->>'target' AS target,
    ad,
    count(viewer)
FROM viewers,
    targets,
    LATERAL (
        SELECT regexp_split_to_table(viewer->>'contacts', '') AS ad
    ) ads
WHERE viewer->>'demography' ~ definition_to_regexp(target->>'definition') AND ad != ''
GROUP BY date, target, ad;
