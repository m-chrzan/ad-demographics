CREATE TABLE audience_json (date DATE NOT NULL, audience JSONB NOT NULL);
\set content `cat input/audience-2019-01-01.json`
INSERT INTO audience_json VALUES ('2019-01-01', :'content');
\set content `cat input/audience-2019-01-02.json`
INSERT INTO audience_json VALUES('2019-01-02', :'content');
\set content `cat input/audience-2019-01-03.json`
INSERT INTO audience_json VALUES('2019-01-03', :'content');
\set content `cat input/audience-2019-01-04.json`
INSERT INTO audience_json VALUES('2019-01-04', :'content');
\set content `cat input/audience-2019-01-05.json`
INSERT INTO audience_json VALUES('2019-01-05', :'content');
\set content `cat input/audience-2019-01-06.json`
INSERT INTO audience_json VALUES('2019-01-06', :'content');
\set content `cat input/audience-2019-01-07.json`
INSERT INTO audience_json VALUES('2019-01-07', :'content');
\set content `cat input/audience-2019-01-08.json`
INSERT INTO audience_json VALUES('2019-01-08', :'content');
\set content `cat input/audience-2019-01-09.json`
INSERT INTO audience_json VALUES('2019-01-09', :'content');
\set content `cat input/audience-2019-01-10.json`
INSERT INTO audience_json VALUES('2019-01-10', :'content');
\set content `cat input/audience-2019-01-11.json`
INSERT INTO audience_json VALUES('2019-01-11', :'content');
\set content `cat input/audience-2019-01-12.json`
INSERT INTO audience_json VALUES('2019-01-12', :'content');
\set content `cat input/audience-2019-01-13.json`
INSERT INTO audience_json VALUES('2019-01-13', :'content');
\set content `cat input/audience-2019-01-14.json`
INSERT INTO audience_json VALUES('2019-01-14', :'content');
\set content `cat input/audience-2019-01-15.json`
INSERT INTO audience_json VALUES('2019-01-15', :'content');
\set content `cat input/audience-2019-01-16.json`
INSERT INTO audience_json VALUES('2019-01-16', :'content');
\set content `cat input/audience-2019-01-17.json`
INSERT INTO audience_json VALUES('2019-01-17', :'content');
\set content `cat input/audience-2019-01-18.json`
INSERT INTO audience_json VALUES('2019-01-18', :'content');
\set content `cat input/audience-2019-01-19.json`
INSERT INTO audience_json VALUES('2019-01-19', :'content');
\set content `cat input/audience-2019-01-20.json`
INSERT INTO audience_json VALUES('2019-01-20', :'content');
\set content `cat input/audience-2019-01-21.json`
INSERT INTO audience_json VALUES('2019-01-21', :'content');
\set content `cat input/audience-2019-01-22.json`
INSERT INTO audience_json VALUES('2019-01-22', :'content');
\set content `cat input/audience-2019-01-23.json`
INSERT INTO audience_json VALUES('2019-01-23', :'content');
\set content `cat input/audience-2019-01-24.json`
INSERT INTO audience_json VALUES('2019-01-24', :'content');
\set content `cat input/audience-2019-01-25.json`
INSERT INTO audience_json VALUES('2019-01-25', :'content');
\set content `cat input/audience-2019-01-26.json`
INSERT INTO audience_json VALUES('2019-01-26', :'content');
\set content `cat input/audience-2019-01-27.json`
INSERT INTO audience_json VALUES('2019-01-27', :'content');
\set content `cat input/audience-2019-01-28.json`
INSERT INTO audience_json VALUES('2019-01-28', :'content');
\set content `cat input/audience-2019-01-29.json`
INSERT INTO audience_json VALUES('2019-01-29', :'content');
\set content `cat input/audience-2019-01-30.json`
INSERT INTO audience_json VALUES('2019-01-30', :'content');

CREATE TABLE targets_json (targets JSONB NOT NULL);
\copy targets_json from 'input/targets.json'
