SELECT viewers.date, target, ad, count(viewers.person_id)
FROM targets, viewers, views
WHERE
    demography ~ definition_to_regexp(definition) AND
    viewers.person_id = views.person_id AND
    viewers.date = views.date
GROUP BY viewers.date, target, ad;
