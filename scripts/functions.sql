CREATE OR REPLACE FUNCTION definition_to_regexp(text) RETURNS text AS $$
    SELECT regexp_replace($1, ' ', '.', 'g');
$$ LANGUAGE SQL;
