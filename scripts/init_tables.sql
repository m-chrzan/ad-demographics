CREATE TABLE targets (target TEXT NOT NULL, definition TEXT NOT NULL);

INSERT INTO targets
SELECT
    t->>'target' as target,
    t->>'definition' as definition
FROM targets_json, LATERAL (SELECT jsonb_array_elements(targets) as t) ts;

CREATE TABLE viewers_full (
    date TEXT NOT NULL,
    person_id TEXT NOT NULL,
    demography TEXT NOT NULL,
    contacts TEXT NOT NULL
);

INSERT INTO viewers_full
SELECT
    date,
    aud->>'person_id' as person_id,
    aud->>'demography' as demography,
    aud->>'contacts' as contacts
FROM audience_json, LATERAL (SELECT jsonb_array_elements(audience) as aud) auds;

CREATE TABLE viewers (
    date TEXT NOT NULL,
    person_id TEXT NOT NULL,
    demography TEXT NOT NULL
);

INSERT INTO viewers
SELECT date, person_id, demography
FROM viewers_full;

CREATE TABLE views (
    date TEXT NOT NULL,
    person_id TEXT NOT NULL,
    ad TEXT NOT NULL
);

INSERT INTO views
SELECT date, person_id, ad
FROM viewers_full, LATERAL (
    WITH ads AS (SELECT regexp_split_to_table(contacts, '') as ad)
    SELECT * FROM ads WHERE ad != ''
) ads;
